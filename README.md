Low and slow smoked BBQ catered, delivered, curbside, or dine in since 2003. Think Righteous Que for your next corporate or backyard wedding event. Located in the heart of East Cobb, Righteous 'Que combines a love of homemade BBQ with a passion for giving back to the Metro-Atlanta community.

Address: 1050 E Piedmont Rd, Marietta, GA 30062, USA

Phone: 678-221-4783

Website: http://www.righteousque.com
